const express = require("express");
const cors = require("cors");
const {
  validateRequest,
  validateToken,
  validateAuth,
} = require("./validators");
const { deleteAll, searchAll, usersCollection } = require("./database");
const generateId = require("./helpers");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));
  api.get("/", (req, res) => res.json("Hello, World!"));

  api.post("/users", async (req, res) => {
    const { name, password, email } = req.body;
    const users = usersCollection(mongoClient);

    if (validateRequest(req.body, res)) {
      const user = { name, email, password };
      const id = generateId(users);
      const event = JSON.stringify({
        eventType: "UserCreated",
        entityId: id,
        entityAggregate: user,
      });

      stanConn.publish("users", event);

      res.status(201);
      res.send({ user: user });
    }
  });

  api.delete("/users/:uuid", async (req, res) => {
    if (validateAuth(req, res)) {
      const id = req.params.uuid;
      const token = req.header("Authentication").split(" ")[1];
      const users = usersCollection(mongoClient);

      if (validateToken(token, id, secret, res)) {
        const event = JSON.stringify({
          eventType: "UserDeleted",
          entityId: id,
          entityAggregate: {},
        });

        stanConn.publish("users", event);
        res.status(200);
        res.send({ id });
      } else {
        res.status(403);
        res.send({ error: "Access token did not match User ID" });
      }
    }
  });

  // Helper routes for testing purposes
  api.get("/users/deleteAll", async (req, res) => {
    const users = usersCollection(mongoClient);
    await deleteAll(users);
    res.send();
  });

  api.get("/users/", async (req, res) => {
    const users = usersCollection(mongoClient);
    const documents = await searchAll(users);
    const documentsArray = await documents.toArray();
    console.log(documentsArray);
    res.send();
  });

  return api;
};
