const { v4: uuid } = require("uuid");

const generateId = async (collection) => {
  let id = uuid();
  while (await collection.findOne({ _id: id })) {
    id = uuid();
  }
  return id;
};

module.exports = generateId;
