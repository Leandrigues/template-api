const stan = require("node-nats-streaming");
const { usersCollection, insertOne, deleteOne } = require("./database");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    const opts = conn.subscriptionOptions().setStartWithLastReceived;
    const subscription = conn.subscribe("users", opts, (err) => {
      console.log(err);
    });
    subscription.on("message", (msg) => {
      let event = JSON.parse(msg.getData());
      const users = usersCollection(mongoClient);

      if (event.eventType === "UserCreated") {
        const data = event.entityAggregate;
        insertOne(users, data);
      }
      if (event.eventType === "UserDeleted") {
        const uuid = event.entityId;
        deleteOne(users, uuid);
      }
    });
  });

  return conn;
};
