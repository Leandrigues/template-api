const generateId = require("./helpers");

const db = (mongoClient) => {
  try {
    return mongoClient.db(process.env.COMPOSE_PROJECT_NAME);
  } catch (e) {
    throw e;
  }
};

const usersCollection = (mongoClient) => {
  return db(mongoClient).collection("users");
};

const insertOne = async (collection, data) => {
  const id = await generateId(collection);
  const result = await collection.insertOne({
    _id: id,
    ...data,
  });
  return result;
};

const deleteOne = async (collection, id) => {
  await collection.deleteOne({ _id: id });
};

const findOne = async (collection, id) => {
  const result = await collection.findOne({ _id: id });

  return result;
};

const deleteAll = async (collection) => {
  await collection.remove();
};

const searchAll = async (collection) => {
  return await collection.find({});
};

module.exports = {
  insertOne,
  deleteOne,
  findOne,
  usersCollection,
  deleteAll,
  searchAll,
};
