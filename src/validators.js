const jwt = require("jsonwebtoken");

const validateRequest = (body, res) => {
  return (
    validateBody(body, res) &&
    validatePassword(body.password, res) &&
    validadePasswordConfirmation(
      body.password,
      body.passwordConfirmation,
      res
    ) &&
    validateEmail(body.email, res)
  );
};

const validatePassword = (password, res) => {
  if (password.length < 8 || password.length > 12) {
    message = { error: `Request body had malformed field {password}` };
    res.status(400);
    res.send(message);
    return false;
  }

  return true;
};

const validateBody = (body, res) => {
  const requiredFields = ["name", "email", "password", "passwordConfirmation"];
  for (field of requiredFields) {
    if (!(field in body)) {
      message = { error: `Request body had missing field {${field}}` };
      res.status(400);
      res.send(message);
      return false;
    }
  }

  return true;
};

const validateEmail = (email, res) => {
  const regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (!regex.test(email)) {
    message = { error: `Request body had malformed field {email}` };
    res.status(400);
    res.send(message);
    return false;
  }

  return true;
};

const validadePasswordConfirmation = (password, passwordConfirmation, res) => {
  if (password !== passwordConfirmation) {
    message = { error: "Password confirmation did not match" };
    res.status(422);
    res.send(message);
    return false;
  }

  return true;
};

const validateToken = (token, uuid, secret, res) => {
  try {
    const { id } = jwt.verify(token, secret);
    return id == uuid ? true : false;
  } catch (e) {
    res.status(403);
    res.send({ error: "Access Token did not match User ID" });
    throw e;
  }
};

const validateAuth = (req, res) => {
  if (!req.params.uuid) {
    res.send({ error: "'uuid' parameter must be passed" });
    return false;
  }
  if (req.header("Authentication")) {
    return true;
  } else {
    res.status(401);
    res.send({ error: "Access token not found" });
    return false;
  }
};

module.exports = { validateRequest, validateToken, validateAuth };
